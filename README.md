# Announcements

The announcements for the Gitea dashboard are managed here in a simple TOML file. There can only be 3 announcements shown at a given time.

## Syntax

The announcements listed in the file `announcements.toml` will show up (as configured) in the Gitea dashboard for all users.

Properties:  
`timestamp` (required): the creation date and ID of the announcement  
`classes`: additional alert classes, see https://www.gethalfmoon.com/docs/alerts/  
`visible`: if it should be visible; defaults to true  
`visibleUntil`: hide after a certain date automatically  
`dismissable`: if it should be dismissable; defaults to true  
*Not implemented (Sequences):*  
`sequencePredecessor`: show this only after dismissing the announcement with the timestamp/ID referenced here  
`sequenceInterval`: wait this amount of hours after dismissing sequencePredecessor  
*Sequences shall be used to not overwhelm new users while still allowing to tell them about our most important features.*


Translation:
`en|de|...`: markdown-formatted translated contents

Features:
use `@[text](https://...)` to add a button  
Date format: yyyy-mm-ddThh:mm:ssZ (see https://toml.io/en/v1.0.0#offset-date-time)

## Example
~~~
[[announcement]]
timestamp = 2021-02-31T00:00:00Z
classes = ["alert-primary"]
visibleUntil = 2022-02-31T23:00:00Z

en = """
# Today does not exist.
Did you know that your date is invalid?
@[Read more here](https://feb31.invalid)
"""

de = """
# Heute gibt es nicht.
Wusstest du schon, dass es heute gar nicht gibt?
@[Mehr lesen](https://feb31.invalid)
"""

fr = """
# Il n'y a pas aujourd'hui.
Saives-vous que aujourd'hui n'existe pas?
@[Lire tout ici](https://perdu.com)
"""
~~~
